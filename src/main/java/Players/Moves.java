package Players;

public enum Moves {
    SCISSORS,
    ROCK,
    PAPER;


    public boolean beats(Moves another) {
        switch (this) {
            case ROCK:
                return another == SCISSORS;
            case PAPER:
                return another == ROCK;
            case SCISSORS:
                return another == PAPER;
            default:
                throw new IllegalStateException();
        }

    }

}
