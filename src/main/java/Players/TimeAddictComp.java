package Players;


import java.time.LocalTime;


public class TimeAddictComp implements Tactic{
    @Override
    public Moves tactic(Object p) {
        LocalTime localTime = LocalTime.now();
        int sec = localTime.getSecond();

        if (sec % 3 == 1){
            return Moves.SCISSORS;
        } else if (sec % 3 == 0){
            return Moves.PAPER;
        }else {
            return Moves.ROCK;
        }

    }
}
