package Players;

public interface Tactic<T>{
    Moves tactic(T p);
}
