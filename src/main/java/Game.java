import Players.*;
import java.util.*;

public class Game {
    static Scanner scanner = new Scanner(System.in);
    private List<Player> playerList = new ArrayList<>();
    private List<Player> winnersList = new ArrayList<>();
    private List<Player> losersList = new ArrayList<>();
    private Tournament tournament;

    public Game(Tournament tournament) {
        this.tournament = tournament;
    }


    public void gamePlayers() {
        if (playerList.isEmpty()) {
            System.out.println("Enter your playerID:");
            scanner.nextLine();
            String playerID = scanner.nextLine();

            Player realPerson = new Player.PlayerBuilder(playerID, new Realperson()).builder();

            Player timeAddict = new Player.PlayerBuilder("TimeAddict", new TimeAddictComp()).builder();
            Player vowelsAddict = new Player.PlayerBuilder("VowelsAddict", new VowelsAddictComp()).builder();
            Player dumbComputer = new Player.PlayerBuilder("Dumbcomputer", new DumbComp()).builder();


            playerList.add(timeAddict);
            playerList.add(vowelsAddict);
            playerList.add(dumbComputer);
            playerList.add(realPerson);
            tournament.setPlayerList(playerList);
        } else {
            System.out.println("Start new tournament with same players");
        }
    }

    public void gameON() {
        gamePlayers();

        Collections.shuffle(playerList);
        Player player1 = playerList.get(0);
        Player player2 = playerList.get(1);

        Player player3 = playerList.get(2);
        Player player4 = playerList.get(3);


        firstMatchUps(player1, player2);
        System.out.println("-----------------------");
        firstMatchUps(player3, player4);
        theFinals();


    }

    public void firstMatchUps(Player player1, Player player2) {
        System.out.println("Best of one!\n" + player1.getName() + " VS " + player2.getName());

        while (true) {
            Moves playerOneMove = player1.getTactic().tactic(player2.getName());
            Moves playerTwoMove = player2.getTactic().tactic(player1.getName());
            System.out.println(player1.getName() + " chose " + playerOneMove);
            System.out.println(player2.getName() + " chose " + playerTwoMove);
            if (playerOneMove == playerTwoMove) {
                System.out.println("It´s a tie!\n The match will continue.");
            } else if (playerOneMove.beats(playerTwoMove)) {
                System.out.println(player1.getName() + " has won the mathcup VS " + player2.getName());
                System.out.println(player1.getName() + " will continue to the final and " + player2.getName() + " will fight for bronze!");
                winnersList.add(player1);
                losersList.add(player2);
                break;
            } else {
                System.out.println(player2.getName() + " has won the mathcup VS " + player1.getName());
                System.out.println(player2.getName() + " will continue to the final and " + player1.getName() + " will fight for bronze!");
                winnersList.add(player2);
                losersList.add(player1);
                break;
            }
        }

    }

    public void theFinals() {

        Player player1 = winnersList.get(0);
        Player player2 = winnersList.get(1);
        secondMatchUps(player1, player2);
        Player firstPlace = winnersList.get(0);
        firstPlace.setScore(3);
        winnersList.remove(0);
        Player secondPlace = winnersList.get(0);
        secondPlace.setScore(2);

        Player player3 = losersList.get(0);
        Player player4 = losersList.get(1);
        secondMatchUps(player3, player4);
        Player thirdPlace = losersList.get(0);
        thirdPlace.setScore(1);
        losersList.remove(0);
        Player fourthPlace = losersList.get(0);
        fourthPlace.setScore(0);

        System.out.println("------------------------------");

        System.out.println(firstPlace.getName() + " has won GOLD!!!");
        System.out.println(secondPlace.getName() + " has won SILVER!!!");
        System.out.println(thirdPlace.getName() + " has won BRONZE!!!");
        System.out.println(fourthPlace.getName() + " walks home with nothing at all.");

        MatchResult matchResult = new MatchResult();

        String dateAndTime = matchResult.getFormattedDateAndTime();


        matchResult.setMatchResult(dateAndTime, Placement.firstPlace(firstPlace), Placement.secondPlace(secondPlace),
                Placement.thirdPlace(thirdPlace), Placement.fourthPlace(fourthPlace));
        System.out.println("-----------------------");


        tournament.addMatchResult(matchResult);


        winnersList.clear();
        losersList.clear();

    }

    public void secondMatchUps(Player player1, Player player2) {
        System.out.println("Best of one!\n" + player1.getName() + " VS " + player2.getName());

        while (true) {
            Moves playerOneMove = player1.getTactic().tactic(player2.getName());
            Moves playerTwoMove = player2.getTactic().tactic(player1.getName());
            System.out.println(player1.getName() + " chose " + playerOneMove);
            System.out.println(player2.getName() + " chose " + playerTwoMove);
            if (playerOneMove == playerTwoMove) {
                System.out.println("It´s a tie!\n The match will continue.");

            } else if (playerOneMove.beats(playerTwoMove)) {
                System.out.println(player1.getName() + " has won the mathcup VS " + player2.getName());


                break;
            } else if (playerTwoMove.beats(playerOneMove)){
                System.out.println(player2.getName() + " has won the mathcup VS " + player1.getName());

                break;
            }


        }

    }


}
