import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class MatchResult {
    public enum Medal{
        GOLD,
        SILVER,
        BRONZE,
        LAST;
    }

    private Placement firstPlace;
    private Placement secondPlace;
    private Placement thirdPlace;
    private Placement fourthPlace;
    private String localDateTime;



    public void setMatchResult(String localDateTime, Placement firstPlace, Placement secondPlace, Placement thirdPlace, Placement fourthPlace){
        this.localDateTime = localDateTime;
        this.firstPlace = firstPlace;
        this.secondPlace = secondPlace;
        this.thirdPlace = thirdPlace;
        this.fourthPlace = fourthPlace;
    }

    public DateTimeFormatter getMyFormat(){

        return DateTimeFormatter.ofPattern("yyyy/MM-dd HH:mm");
    }
    public String getFormattedDateAndTime(){

        return LocalDateTime.now().format(getMyFormat());

    }
    @Override
    public String toString() {
        return "Date: " + localDateTime +
                "\n Tournamentresult:" +
                "\n " + firstPlace +
                "\n " + secondPlace +
                "\n " + thirdPlace +
                "\n " + fourthPlace+
                "\n -------------";
    }


    public Placement getFirstPlace() {
        return firstPlace;
    }

    public Placement getFourthPlace() {
        return fourthPlace;
    }

    public Placement getThirdPlace() {
        return thirdPlace;
    }

    public Placement getSecondPlace() {
        return secondPlace;
    }

}
