import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tournament {
    private List<MatchResult> matchResults = new ArrayList<>();
    private List<Player> playerList;

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public void printMatchResults(){

        matchResults.forEach(System.out::println);


    }
    public void addMatchResult(MatchResult matchResult) {

        matchResults.add(matchResult);
    }

    public List<MatchResult> getMatchResults() {

        return matchResults;
    }

    public void printPlayerStats(){


        System.out.println("3 points = gold\n" + "2 points = silver\n" + "1 point = bronze\n" + "0 points = last place");
        System.out.println();
        for (Player p:playerList) {
            double averageScore = (double) p.getScore() / (double) matchResults.size();
            System.out.println("Playerstats for " + p.getName());
            System.out.println("Average score is " + averageScore);
            List<MatchResult.Medal> playersPlacement = matchResults.stream()
                    .map(matchResult -> List.of(matchResult.getFirstPlace(), matchResult.getSecondPlace(), matchResult.getThirdPlace(), matchResult.getFourthPlace()))
                    .flatMap(List::stream)
                    .filter(placement -> placement.getPlayer().equals(p))
                    .map(Placement::getMedal).sorted(Comparator.naturalOrder()).collect(Collectors.toList());

            MatchResult.Medal bestPlacement = playersPlacement.get(0);
            playersPlacement.sort(Comparator.reverseOrder());
            MatchResult.Medal worstPlacement = playersPlacement.get(0);
            System.out.println("Best placement is " + bestPlacement);
            System.out.println("Worst placement is " + worstPlacement);

           

        }


    }





}
