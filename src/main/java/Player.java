import Players.Tactic;


public class Player {

    private String name;
    private Tactic tactic;
    private int score = 0;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {

        this.score += score;
    }

    private Player(PlayerBuilder playerBuilder){
        this.name = playerBuilder.name;
        this.tactic = playerBuilder.tactic;
    }

    static class PlayerBuilder{
        private String name;
        private Tactic tactic;

        public PlayerBuilder(String name, Tactic tactic){
            this.name = name;
            this.tactic = tactic;
        }



        public Player builder(){
            return new Player(this);

        }
    }

    public Tactic getTactic() {

        return tactic;
    }

    public String getName() {

        return name;
    }


}
