public class Placement {

    private MatchResult.Medal medal;
    private Player player;

    private Placement(Player player, MatchResult.Medal medal) {
        this.player = player;
        this.medal = medal;
    }
    public static Placement firstPlace(Player player){
        return new Placement(player, MatchResult.Medal.GOLD);
    }
    public static Placement secondPlace(Player player){
        return new Placement(player, MatchResult.Medal.SILVER);
    }
    public static Placement thirdPlace(Player player){
        return new Placement(player, MatchResult.Medal.BRONZE);
    }
    public static Placement fourthPlace(Player player){
        return new Placement(player, MatchResult.Medal.LAST);
    }

    @Override
    public String toString() {
        return medal.name() + " " + player.getName();
    }

    public MatchResult.Medal getMedal() {
        return medal;
    }

    public Player getPlayer() {
        return player;
    }
}
